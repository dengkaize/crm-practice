# CRM练习
一、项目环境
1、JDK1.8
2、Git 2.25
3、Mysql 5.0
4、IDEA2020  或 MyEclipse10
5、数据库可视化工具Navicat
二、项目实现功能
1、客户信息添加、客户信息列表、客户信息修改删除
2、联系人信息添加、联系人信息列表、联系人信息修改删除
3、拜访信息信息添加、拜访信息列表、拜访信息修改删除