package com.test.entity;

public class CustomerModel {

	private String cid;
	private String cname;
	private String csource;
	private String caddress;
	private String cphone;
	private LevelModel levelModel;
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCsource() {
		return csource;
	}
	public void setCsource(String csource) {
		this.csource = csource;
	}
	
	public String getCaddress() {
		return caddress;
	}
	public void setCaddress(String caddress) {
		this.caddress = caddress;
	}
	public String getCphone() {
		return cphone;
	}
	public void setCphone(String cphone) {
		this.cphone = cphone;
	}
	public LevelModel getLevelModel() {
		return levelModel;
	}
	public void setLevelModel(LevelModel levelModel) {
		this.levelModel = levelModel;
	}
	@Override
	public String toString() {
		return "CustomerModel [cid=" + cid + ", cname=" + cname + ", csource="
				+ csource + ", caddress=" + caddress + ", cphone=" + cphone
				+ ", levelModel=" + levelModel + "]";
	}

	
}
