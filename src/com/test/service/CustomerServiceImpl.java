package com.test.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.dao.CustomerDao;
import com.test.entity.CustomerModel;
import com.test.entity.LevelModel;
@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerDao customerDao;
	
    //查询所有级别
		public List<LevelModel> findAllLevel() {
		// TODO Auto-generated method stub
		return this.customerDao.findAllLevel();
	}
	//添加客户
	public void add(CustomerModel customerModel) {
		String cid = UUID.randomUUID().toString();
		customerModel.setCid(cid);
		this.customerDao.add(customerModel);
		
	}
	//获取客户列表
	public List<CustomerModel> findAll() {
		return this.customerDao.findAll();
	}
	//删除客户
	public void delete(String cid) {
		this.customerDao.delete(cid);
	}
	//通过cid查询客户
	public CustomerModel findOne(CustomerModel customerModel) {
		return this.customerDao.findOne(customerModel);
	}
	//更新客户信息
	public void update(CustomerModel customerModel) {
		this.customerDao.update(customerModel);
	}
	

}