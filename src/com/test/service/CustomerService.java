package com.test.service;

import java.util.List;

import com.test.entity.CustomerModel;
import com.test.entity.LevelModel;

public interface CustomerService {
	//查询所有级别  （泛型：数据类型的参数化）
	List<LevelModel> findAllLevel();
	//添加客户
	void add(CustomerModel customerModel);
	//查询客户列表
	List<CustomerModel> findAll();
	//删除客户
	void delete(String cid);
	//通过cid查询客户
	CustomerModel findOne(CustomerModel customerModel);
	//修改客户信息
	void update(CustomerModel customerModel);

}