package com.test.actoin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.test.entity.CustomerModel;
import com.test.entity.LevelModel;
import com.test.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerAction {
	@Autowired
	private CustomerService customerService;
	
	//跳转到添加界面
	@RequestMapping("/toAddPage")
	public String toAddPage(){
		return "customer/add";
	}
	
	//查询级别列表
	@RequestMapping("/allLevelJson")
	public String allLevelJson(HttpServletResponse response) throws IOException{
		List<LevelModel> list = this.customerService.findAllLevel();
		String json =JSON.toJSONString(list);
		response.setContentType("application/json;charset=utf-8");
		response.getWriter().write(json);
		return null;

	}
	//添加客户
	@RequestMapping("/addCustomer")
	public String addCustomer(CustomerModel customerModel,HttpServletRequest request){
		this.customerService.add(customerModel);
		return "success";
	}
	//显示客户列表
	@RequestMapping("/list")
	public String list(Model model){	
		List<CustomerModel> list = this.customerService.findAll();
		model.addAttribute("list" ,list);
		return "customer/list";
	}	
	//删除该客户逻辑删除
	@RequestMapping("/deleteCustomer")
	public String deleteCustomer(HttpServletRequest request){
		String cid = request.getParameter("cid");
		this.customerService.delete(cid);
		return "success";
	}
	//跳转到修改客户信息界面（根据cid进行查询）
	@RequestMapping("/toUpdatePage")
	public String toUpdatePage(CustomerModel customerModel,Model model){
		CustomerModel customer = this.customerService.findOne(customerModel);
		model.addAttribute("customer",customer);
		return "customer/edit";
	}	
	//修改客户信息
	@RequestMapping("/updateCustomer")
	public String updateCustomer(CustomerModel customerModel){
		this.customerService.update(customerModel);
		return "redirect:/customer/list.action";
	}
}