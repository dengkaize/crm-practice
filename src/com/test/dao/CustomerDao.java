package com.test.dao;

import java.util.List;

import com.test.entity.CustomerModel;
import com.test.entity.LevelModel;

public interface CustomerDao {
	//查询所有级别  （泛型：数据类型的参数化）
	List<LevelModel> findAllLevel();
	//添加客户
	void add(CustomerModel customerModel);
	//查询客户列表
	List<CustomerModel> findAll();
	//根据cid删除客户
	void delete(String cid);
	//根据cid查询客户
	CustomerModel findOne(CustomerModel customerModel);
	//修改客户
	void update(CustomerModel customerModel);

}
